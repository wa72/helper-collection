<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Wa72\HelperCollection\FilesystemHelper;
use function Wa72\HelperCollection\merge_paths;

/**
 * Created by:
 * User: Matthias Koch
 * Date: 15.03.2022
 * Time: 14:55
 */
class FilesystemHelperTest extends TestCase
{
    public function testSanitizeFileName(): void
    {
        $tests = [
            [
                'file-name' => 'abc.pdf',
                'expected'  => 'abc.pdf',
            ],
            [
                'file-name' => 'Äbc.pdf',
                'expected'  => 'Aebc.pdf',
            ],
            [
                'file-name' => 'ab*{.gz',
                'expected'  => 'ab.gz',
            ],
            [
                'file-name' => 'a  bcd.pdf',
                'expected'  => 'a-bcd.pdf',
            ],
            [
                'file-name' => '/tmp/abc.log',
                'expected'  => 'tmpabc.log',
            ],
            [
                'file-name' => 'abc_de.pdf',
                'expected'  => 'abc_de.pdf',
            ],
        ];

        foreach ($tests as $test) {
            $fileName = $test['file-name'];
            $expected = $test['expected'];

            $actual = FilesystemHelper::sanitizeFileName($fileName);

            $this->assertEquals($expected, $actual);
        }
    }


    public function testFunction_merge_paths(): void
    {
        $this->assertEquals(
            'stackoverflow.com/questions',
            merge_paths('stackoverflow.com', 'questions')
        );

        $this->assertEquals(
            'usr/bin/perl/',
            merge_paths('usr/bin/', '/perl/')
        );

        $this->assertEquals(
            'en.wikipedia.org/wiki/Sega_32X',
            merge_paths('en.wikipedia.org/', '/wiki', ' Sega_32X')
        );

        $this->assertEquals(
            'etc/apache/php',
            merge_paths('etc/apache/', '', '/php')
        );

        $this->assertEquals(
            '/webapp/api',
            merge_paths('/', '/webapp/api')
        );

        $this->assertEquals(
            '/webapp/api',
            merge_paths('/', '/webapp/api')
        );

        $this->assertEquals(
            '/webapp2/api',
            merge_paths('', '/webapp2/api')
        );

        $this->assertEquals(
            'http://google.com/',
            merge_paths('http://google.com', '/', '/')
        );

        $this->assertEquals(
            'subfolder',
            merge_paths('', 'subfolder')
        );
    }


    public function testAppendsNoSlashAtBeginning(): void
    {
        $this->assertEquals(
            'stackoverflow.com/questions',
            FilesystemHelper::merge_paths('stackoverflow.com', 'questions')
        );

        $this->assertEquals(
            'usr/bin/perl/',
            FilesystemHelper::merge_paths('usr/bin/', '/perl/')
        );

        $this->assertEquals(
            'en.wikipedia.org/wiki/Sega_32X',
            FilesystemHelper::merge_paths('en.wikipedia.org/', '/wiki', ' Sega_32X')
        );

        $this->assertEquals(
            'etc/apache/php',
            FilesystemHelper::merge_paths('etc/apache/', '', '/php')
        );

        $this->assertEquals(
            '/webapp/api',
            FilesystemHelper::merge_paths('/', '/webapp/api')
        );

        $this->assertEquals(
            '/webapp/api',
            FilesystemHelper::merge_paths('/', '/webapp/api')
        );

        $this->assertEquals(
            '/webapp2/api',
            FilesystemHelper::merge_paths('', '/webapp2/api')
        );

        $this->assertEquals(
            'http://google.com/',
            FilesystemHelper::merge_paths('http://google.com', '/', '/')
        );

        $this->assertEquals(
            'subfolder',
            FilesystemHelper::merge_paths('', 'subfolder')
        );
    }
}
