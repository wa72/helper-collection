<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 20.07.2022
 * Time: 11:24
 */

namespace Wa72\HelperCollection;

class EnvironmentHelper
{
    private static ?bool $isDdevProject;

    // IS_DDEV_PROJECT
    public static function isDdevProject(): bool
    {
        if(isset(self::$isDdevProject)) {
            return self::$isDdevProject;
        }

        // first: check environment variables provided inside container
        if (false !== getenv('IS_DDEV_PROJECT', true)) {
            if('true' === getenv('IS_DDEV_PROJECT', true)) {
                self::$isDdevProject = true;
            } else {
                self::$isDdevProject = false;
            }

            return self::$isDdevProject;
        }

        // second: check for location of .ddev folder
        try {
            self::findRootDir('.ddev');
            self::$isDdevProject = true;
        } catch (\Exception $e) {
            self::$isDdevProject = false;
        }

        return self::$isDdevProject;
    }

    public static function findRootDir(string $rootDirIndicator = 'composer.json'): string
    {
        $root = null;
        $directory = dirname(__FILE__);

        // Go up until you find a composer.json file
        // which should exist in the ancestors
        // because it's a composer package.
        do {
            $directory = dirname($directory);
            $composer = merge_paths($directory, $rootDirIndicator);
            if (file_exists($composer)) {
                $root = $directory;
            }
        } while (is_null($root) && $directory != '/');

        // We either are at the root or we got lost.
        // i.e. a composer.json was nowhere to be found.
        if (!is_null($root)) {
            // Yay! we are at the root.
            // and $root contains the path.
            // Do whatever you seem fit!
            return $root;
        } else {
            // Oh no! Can we default to something?
            // Or just bail out?
            throw new \Exception('Oops, did you require this package via composer?');
        }
    }
}
