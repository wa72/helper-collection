<?php

namespace Wa72\HelperCollection;

function merge_paths(string ... $paths): string {
    // use Argument Unpacking
    return FilesystemHelper::merge_paths(... $paths);
}

function removeTrailingSlash(string $path): string {
    return FilesystemHelper::removeTrailingSlash($path);
}

function addTrailingSlash(string $path): string {
    return FilesystemHelper::addTrailingSlash($path);
}
