<?php declare(strict_types=1);
/**
 * Created by:
 * User: Matthias Koch
 * Date: 11.03.2022
 * Time: 10:37
 */

namespace Wa72\HelperCollection;

class FilesystemHelper
{
    public static function sanitizeFileName(string $fileName, bool $replaceGermanUmlaute = true): string
    {
        // Remove multiple spaces
        $fileName = preg_replace('/\s+/', ' ', $fileName);

        // Replace spaces with hyphens
        $fileName = preg_replace('/\s/', '-', $fileName);

        if($replaceGermanUmlaute) {
            // Replace german characters
            $germanReplaceMap = [
                'ä' => 'ae',
                'Ä' => 'Ae',
                'ü' => 'ue',
                'Ü' => 'Ue',
                'ö' => 'oe',
                'Ö' => 'Oe',
                'ß' => 'ss',
            ];
            $fileName = str_replace(array_keys($germanReplaceMap), $germanReplaceMap, $fileName);
        }

        // Remove everything but "normal" characters
        $fileName = preg_replace("([^\w\s\d\-\.])", '', $fileName);

        // Remove multiple hyphens
        $fileName = preg_replace('/-+/', '-', $fileName);

        return $fileName;
    }

    /**
     * Merge several parts of URL or filesystem path in one path
     * Examples:
     *  echo merge_paths('stackoverflow.com', 'questions');           // 'stackoverflow.com/questions' (slash added between parts)
     *  echo merge_paths('usr/bin/', '/perl/');                       // 'usr/bin/perl/' (double slashes are removed)
     *  echo merge_paths('en.wikipedia.org/', '/wiki', ' Sega_32X');  // 'en.wikipedia.org/wiki/Sega_32X' (accidental space fixed)
     *  echo merge_paths('etc/apache/', '', '/php');                  // 'etc/apache/php' (empty path element is removed)
     *  echo merge_paths('/', '/webapp/api');                         // '/webapp/api' slash is preserved at the beginnnig
     *  echo merge_paths('http://google.com', '/', '/');              // 'http://google.com/' slash is preserved at the end
     * @param string ...$paths
     * @return string
     */
    public static function merge_paths(string ... $paths): string
    {
        $preserveStartSlash = false;

        $paths =  array_values(array_filter($paths));
        $noPathElements = count($paths);
        if (0 === $noPathElements) {
            return '';
        }

        $last_key = $noPathElements - 1;
        array_walk($paths, function (&$val, $key) use ($last_key, &$preserveStartSlash, $noPathElements) {
            switch ($key) {
                case 0:
                    if (strpos($val, '/') === 0) {
                        $preserveStartSlash = true;
                    }
                    $val = rtrim($val, '/ ');
                    break;
                case $last_key:
                    if (1 == $noPathElements && strpos($val, '/') === 0) {
                        $preserveStartSlash = true;
                    }
                    $val = ltrim($val, '/ ');
                    break;
                default:
                    $val = trim($val, '/ ');
                    break;
            }
        });

        $first = array_shift($paths);
        $last = array_pop($paths);
        $paths = array_filter($paths); // clean empty elements to prevent double slashes
        array_unshift($paths, $first);

        // only add last, if last != first
        if ($noPathElements > 1) {
            $paths[] = $last;
        }

        $merged = implode('/', $paths);
        if ($preserveStartSlash && strpos($merged, '/') !== 0) {
            $merged = '/' . $merged;
        }

        return $merged;
    }

    /**
     * @param string $dir
     * @param int $sorting_order
     * @return false|string[]
     */
    public static function better_scandir(string $dir, int $sorting_order = SCANDIR_SORT_ASCENDING)
    {

        /****************************************************************************/
        // Roll through the scandir values.
        /** @var string[] $files */
        $files = [];
        foreach (scandir($dir, $sorting_order) as $file) {
            if ($file[0] === '.') {
                continue;
            }
            $files[$file] = filemtime($dir . '/' . $file);
        } // foreach

        /****************************************************************************/
        // Sort the files array.
        if (SCANDIR_SORT_ASCENDING == $sorting_order) {
            asort($files, SORT_NUMERIC);
        } else {
            arsort($files, SORT_NUMERIC);
        }

        /****************************************************************************/
        // Set the final return value.
        $ret = array_keys($files);

        /****************************************************************************/

        // Return the final value.
        return ($ret) ? $ret : false;
    }

    public static function addTrailingSlash(string $path): string
    {
        return self::removeTrailingSlash($path) . '/';
    }

    public static function removeTrailingSlash(string $path): string
    {
        return rtrim($path, '/');
    }
}
