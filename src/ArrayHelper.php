<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 11.03.2022
 * Time: 10:00
 */

namespace Wa72\HelperCollection;

class ArrayHelper
{
    public static function wrapEachItem(
        array $array,
        string $prefix = '',
        $suffix = '',
        bool $trimEachItem = true
    ): array {
        return array_map(function ($e) use ($prefix, $suffix, $trimEachItem) {
            if ($trimEachItem) {
                return $prefix . trim($e) . $suffix;
            }

            return "{$prefix}{$e}{$suffix}";
        }, $array);
    }

    public static function wrapEachItemAndImplode(
        array $array,
        string $prefix = '',
        $suffix = '',
        bool $trimEachItem = true,
        bool $trimResult = true
    ): string {
        $wrappedString = implode('', self::wrapEachItem($array, $prefix, $suffix, $trimEachItem));

        if ($trimResult) {
            return trim($wrappedString);
        }

        return trim($wrappedString);
    }

    public static function prettyDump($arr, int $d = 0)
    {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                for ($i = 0; $i < $d; $i++) {
                    echo "   ";
                }
                if (is_array($v)) {
                    echo $k . PHP_EOL;
                    self::prettyDump($v, $d + 1);
                } else {
                    if (GenericHelper::isClosure($v)) {
                        try {
//                            $value = call_user_func($v);
//                            $value = $v->call();
                            $value = 'CLOSURE';
                            if (is_array($value)) {
                                echo $k . PHP_EOL;
                                self::prettyDump($value, $d + 1);
                            } else {
                                echo $k . "\t" . $value . PHP_EOL;
                            }
                        } catch (\Exception $e) {
                            echo $k . PHP_EOL;
                        }
                    } else {
                        $value = $v;
                        if ($v instanceof \DateTime) {
                            $value = $v->format('Y-m-d H:i:s');
                        } else {
                            $value = GenericHelper::formatAsBooleanIfPossible($v);
                        }


                        echo $k . "\t" . $value . PHP_EOL;
                    }
                }
            }
        }
    }

    /**
     * Filter $haystack array items with items from array $patterns.
     * Example usage:
     * filterWithRegexp(['cf_.*', 'bcd'], ['abc', 'cf_test1', 'bcd' ,'cf_test2', 'cde']) will return ['cf_test1', 'bcd', 'cf_test2']
     *
     * SourceBroker\DeployerExtendedDatabase\Utility
     *
     * @param array $patterns
     * @param array $haystack
     * @return array
     */
    public static function filterWithRegexp(array $patterns, array $haystack): array
    {
        $foundItems = [];
        foreach ($patterns as $pattern) {
            $regexp = false;

            set_error_handler(function () {
            }, E_WARNING);
            $isValidPattern = preg_match($pattern, '') !== false;
            $isValidPatternDelimiters = preg_match('/^' . $pattern . '$/', '') !== false;
            restore_error_handler();

            if (preg_match('/^[\/\#\+\%\~]/', $pattern) && $isValidPattern) {
                $regexp = $pattern;
            } elseif ($isValidPatternDelimiters) {
                $regexp = '/^' . $pattern . '$/i';
            }
            if ($regexp) {
                $foundItems = array_merge($foundItems, preg_grep($regexp, $haystack));
            } elseif (in_array($pattern, $haystack)) {
                $foundItems[] = $pattern;
            }
        }

        return $foundItems;
    }
}
