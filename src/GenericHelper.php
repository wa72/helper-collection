<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 11.03.2022
 * Time: 13:01
 */

namespace Wa72\HelperCollection;

class GenericHelper
{
    /**
     * Check "Booleanic" Conditions :)
     *
     * @param  [mixed]  $variable  Can be anything (string, bol, integer, etc.)
     * @return boolean|null            Returns TRUE  for "1", "true", "on" and "yes"
     *                             Returns FALSE for "0", "false", "off" and "no"
     *                             Returns NULL otherwise.
     */
    public static function is_enabled($variable):?bool
    {
        if (!isset($variable)) return null;
        return filter_var($variable, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }

    /**
     * Returns 'true' or 'false' if the variable is a boolean like thing.
     *
     * @param mixed $variable
     * @return mixed|null
     */
    public static function formatAsBooleanIfPossible($variable) {
        $isEnabled = self::is_enabled($variable);
        if(is_bool($isEnabled) || false === $isEnabled || true === $isEnabled) {
            return ($isEnabled ? 'true' : 'false');
        } else {
            return $variable;
        }
    }

    public static function isClosure($var): bool
    {
        return is_object($var) && ($var instanceof \Closure);
    }

}
