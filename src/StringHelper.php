<?php
/**
 * Created by:
 * User: Matthias Koch
 * Date: 11.03.2022
 * Time: 10:45
 */

namespace Wa72\HelperCollection;

if (!function_exists('str_contains')) {
    function str_contains(string $haystack, string $needle): bool
    {
        return empty($needle) || strpos($haystack, $needle) !== false;
    }
}

if (!function_exists('str_starts_with')) {
    function str_starts_with($haystack, $needle): bool
    {
        return strncmp($haystack, $needle, strlen($needle)) === 0;
    }
}

if (!function_exists('str_ends_with')) {
    function str_ends_with($haystack, $needle): bool
    {
        $length = strlen($needle);

        return $length > 0 ? substr($haystack, -$length) === $needle : true;
    }
}

class StringHelper
{
    /**
     * Determine if a given string ends with a given substring.
     *
     * @param string $haystack
     * @param  string|string[]  $needles
     * @return bool
     */
    public static function endsWith(string $haystack, $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if (
                $needle !== '' && $needle !== null
                && str_ends_with($haystack, $needle)
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string $haystack
     * @param  string|string[]  $needles
     * @return bool
     */
    public static function startsWith(string $haystack, $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if ((string) $needle !== '' && str_starts_with($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }
}
